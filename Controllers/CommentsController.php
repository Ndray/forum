<?php


namespace Forum\Controllers;

use Forum\Models\Posts;
use Forum\Views\View;
use Forum\Models\Comments;

class CommentsController extends BaseController
{
    public function index(array $request = null)
    {
       //var_dump($_SESSION); die();
        if(isset($_GET['id'])) {
            $postId = $_GET['id'];
        }
        $comments = Comments::getCommentsByPostId($postId);
        $view = new View();
        $view->render('comments', ["comments" => $comments]);
        return $view;
    }

    public function newComment()
    {
        $date = date('Y-m-d H:i:s');
        $userId = '';
        $comment = '';
        $themeId = '';
        $children = '';
        if(isset($_POST['comment'])) {
            $comment = $_POST['comment'];
           // $themeId = $_POST['comment_id'];
        }
        if(isset($_POST['comment_id'])){
            $themeId = $_POST['comment_id'];
        }
        if (isset($_SESSION['user_id'])){
            $userId = $_SESSION['user_id'];
        }
        if (isset($_GET['id'])){
            $themeId = $_GET['id'];
        }
        $parrentId = 0;
        $children = 0;
        $comments = Comments::addComment($comment, $themeId, (int)$userId, $date, $parrentId, $children );
        return header("location: /");
    }

    function getChildren ($themeId, $parrentId=0)
    {
        $comments = Comments::getChildrenForComment($themeId, $parrentId);
        foreach ($comments as $comment) {
            $commentId = $comment['id'];
            $userId = $comment['user_id'];
            $commentText = $comment['text'];

            //render comment using above variables
            //Use $level to render the intendation level



            $view = new View();
            $view->render('comments', ["comments" => $comments]);
            return $view;
        }
    }



    /*public function display_comments ($themeId, $parrentId=0, $level=0) {
        $comments = Comments::fetchArticleComments($themeId, $parrentId);
        foreach($comments as $comment) {
        $id = $comment['id'];
        $userId = $comment['user_id'];
        $text = $comment['text'];
        $date = $comment['date'];  //get time ago

        //render comment using above variables
        //Use $level to render the intendation level

        //Recurse
        display_comments ($themeId, $id, $level+1);
    }
  return header("location: test");*/


    /*public function buildTree($comments)
    {
        $tree = [];
        foreach ($comments as $id=>&$row){
            if (empty($row['parrent_id'])){
                $tree[$id] = &$row;
            }else {
                $comments[$row['parrent_id']]['child'][$id] = &$row;
            }
        }
        return header("location: /");
        var_dump($tree);
    }*/



    /*public function commentsToPosts()
    {
        $comments = Comments::commentToPost();
        $view = new View();
        $view->render('commentsToPosts', ["comments" => $comments]);
        return $view;
    }*/
}