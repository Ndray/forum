<?php
namespace Forum\Controllers;

use \Forum\Views\View;
use \Forum\Routers\Router;

class BaseController
{
    public $view;
    protected $exception = [];

    public function __construct()
    {
        $this->view = new View();
    }
}
