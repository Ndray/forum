<?php
namespace Forum\Controllers;

class ErrorController extends BaseController
{
    public function error404(array $request = null)
    {
        $this->view->renderFull('error404');
    }
}
