<?php

namespace Forum\Controllers;

use Forum\Models\User;
use Forum\Views\View;
use Forum\Routers\Router;

class UserController extends BaseController
{


    public function login(array $request = null)
    {

        $error = [];
        if (!empty($request)) {
            foreach ($request as $k => $v) {
                if (empty($v)) {
                    $error[$k] = "Field should be filled";
                    continue;
                }
                if ($k === "email" && !filter_var($v, FILTER_VALIDATE_EMAIL)) {
                    $error[$k] = "Email is not valid";
                    continue;
                }
            }

            if (empty($error)) {
                $user = User::login($request['email'], $request['password']);
                if (!empty($user)) {
                    $_SESSION['user_id'] = $user['id'];
                    Router::redirect("/");
                } else {
                    $error['email'] = "Credentials is not valid";
                }
            }
        }
        $this->view->render('login', ['error' => $error]);


    }

    public  function register(array $request = null)
    {
        $error = [];
        if (!empty($request)) {
            foreach ($request as $k => $v) {
                if (empty($v)) {
                    $error[$k] = "Field should be filled";
                    continue;
                }
                if ($k === "email" && !filter_var($v, FILTER_VALIDATE_EMAIL)) {
                    $error[$k] = "Email is not valid";
                    continue;
                }
                if ($k === "email" && User::getUserByEmail($v)){
                    $error[$k] = "Email is used";
                    continue;
                }

                if ($k === "name" && strlen($v) < 4) {
                    $error[$k] = "Name should be longer";
                    continue;
                }
                if ($k === "name" && strlen($v) > 255) {
                    $error[$k] = "Name should be shorter";
                    continue;
                }
            }
            if ($request['password'] != $request['confirm_password']) {
                $error[$k] = "Passwords don't match";
            }

            if (empty($error)) {
                $user = User::register($request['name'], $request['email'], $request['password']);
                if (!empty($user)) {
                    $_SESSION['user_id'] =  $user->id;
                    Router::redirect("/");
                }
            }
        }
        $this->view->render('register', ["error" => $error]);

    }

    public function logout(array $request = null)
    {

        session_destroy();
        Router::redirect("/login");
    }


}