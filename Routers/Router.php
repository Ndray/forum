<?php

namespace Forum\Routers;

//use Forum\Controllers\UserController;


use Forum\Controllers\PostsController;
use Forum\Models\Posts;
use Forum\Controllers\CommentsController;


class Router
{
    private $routes = [];

    public function __construct()
    {
        $this->init();
    }

    public function init()
    {
        $this->setRoute("/", "Posts", "index");
        $this->setRoute("login", "User", "login");
        $this->setRoute("register", "User", "register");
        $this->setRoute("logout", "User", "logout");
        $this->setRoute("comments", "Comments", "index");
        $this->setRoute("add-comments", "Comments", "newComment");
        $this->setRoute("comments", "Comments", "getChildren");
        //$this->setRoute("test", "Comments", "getCategory");
        //$this->setRoute("commentsToPosts", "Comments", "commentsToPosts");
//        $this->setRoute("/", "Comments", "index");

    }

    public function setRoute(string $url, string $controller, string $action): bool
    {
        if (empty($this->routes[$url])) {
            $this->routes[$url] = ["controller" => $controller, "action" => $action];
            return true;
        }
        return false;
    }

    public function getRoute(string $url)
    {
        return $this->routes[$url] ?? [];
    }

    public static function redirect($url)
    {
        header("Location: " . $url);
        die();
    }


    public function process(string $url, array $request)
    {
        $route = $this->getRoute($url);
        if (empty($route)) {
            //404 error
            //  $controller = new \Shop\Controllers\ErrorController();
            //$controller->error404($request);
            return;
        }
        $controllerName = "\\Forum\\Controllers\\" . $route['controller'] . "Controller";
        //self::$currentAction = $route['action'];
        //var_dump($controllerName);
        //var_dump(Posts::getAll());
        $controller = new $controllerName();
        $controller->{$route['action']}($request); // =>PostsController->index($request) <- тоже самое


    }
}
