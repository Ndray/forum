<?php
echo "<pre>";
print_r($stmt);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<p></p>
<style>
    div.comments_wrap{
        width: 600px;
        margin: 0 auto;
    }

    div.comments_wrap ul
    {
        list-style-type: none;
    }

    div.comments_wrap ul li
    {
        margin: 7px 0 7px 7px;
    }

    div.comments_wrap ul li div.comment
    {
        border: solid 1px #ccc;
        padding: 5px 10px;
    }

    div.comments_wrap ul li div.author
    {
        font-weight: bold;
        margin: 3px 0;
    }

    div.comments_wrap ul li div.author span.date
    {
        padding-left: 20px;

    }

    div.comments_wrap ul li div.comment_text
    {
        overflow: auto;
    }
</style>
<div class="comments_wrap">

    <ul>

        <li>

            <div class="comment">

                <div class="user_id">

                    Имя

                    <span class="date">дата</span>

                </div>



                <div class="text">комментарий 1</div>

            </div>

            <ul>

                <li>

                    <div class="comment">

                        <div class="author">

                            Имя

                            <span class="date">дата</span>

                        </div>

                        <div class="comment_text">комментарий 2</div>

                    </div>

                </li>

            </ul>

        </li>

    </ul>

</div>
</body>
</html>

