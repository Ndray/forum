<?php
    define("ROOT_PATH", dirname(__FILE__, 2));
    require_once ROOT_PATH . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

    use Forum\Routers\Router;

    /*define("DB_NAME", "nomework");
    define("DB_USER", "db_user");
    define("DB_PASSWORD", "1111");*/
    $dotenv = Dotenv\Dotenv::createImmutable(ROOT_PATH);
    $dotenv->load();

    //var_dump($dotenv);
/*$a = getenv("DB_NAME");
var_dump($_ENV);
    die();*/
    session_start();

//echo "<pre>";
//var_dump(\Forum\Models\Comments::getAll()); die();
    $request = $_REQUEST ?? [];
    $path = $request['path'] ?? "/";
    unset($request['path']);
    $router = new Router();
    $router->process($path, $request);

?>
