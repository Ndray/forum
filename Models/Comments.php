<?php


namespace Forum\Models;


use Cassandra\Varint;
use PDOException;

class Comments
{
    public $id;
    public $themeId;
    public $userId;
    public $date;
    public $text;
    public $parrentId;
    public $children;

    public function __construct(int $id, int $themeId, int $userId, $date, string $text, string $parrentId, string $children)
    {
        $this->id = $id;
        $this->themeId = $themeId;
        $this->userId = $userId;
        $this->date = $date = date('\'Y-m-d H:i:s\'');
        $this->text = $text;
        $this->parrentId = $parrentId;
        $this->children = $children;

    }

    public static function getAll(): array
    {
        $stmt = Db::getInstance()->query("
            SELECT
	            * 
            FROM
	        comments
	        JOIN posts ON posts.id = comments.theme_id"
        );
        $comments = $stmt->fetchAll();
        foreach ($comments as $comment) {
            $commentNew [] = new Comments(
                $comment['id'],
                $comment['theme_id'],
                $comment['user_id'],
                $comment['date'],
                $comment['text'],
                $comment['parrent_id'],
                $comment['children']
            );
        }
        return $commentNew;
    }

    public static function addComment(string $comment, string $postId, int $userId, $date, string $parrentId, string $children)
    {
        $stmt = Db::getInstance()->prepare('INSERT INTO comments (user_id, theme_id, text, date, parrent_id, children) VALUES (?,?,?,?,?,?)');
        $res = $stmt->execute([$userId, (int)$postId, $comment, $date, $parrentId, $children]);
//        echo "<pre>";
//        print_r($_POST);
        //die();
        return true;
    }


    public function getCommentsByPostId($themeId):array
    {
        $stmt = Db::getInstance()->query("
            SELECT
            	*  
            FROM
	            comments
	        WHERE theme_id = $themeId AND parrent_id = 0"
        );
        $comments = $stmt->fetchAll();
        foreach ($comments as $comment) {
            $commentsByPostId [] = new Comments(
                $comment['id'],
                $comment['theme_id'],
                $comment['user_id'],
                $comment['date'],
                $comment['text'],
                $comment['parrent_id'],
                $comment['children']
            );
        }
        return $commentsByPostId;
    }

    public function getChildrenForComment($themeId, $parrentId)
    {
        if ($parrentId > 0) {
            $parrentId = "=$parrentId";
        } else {
            $parrentId = "IS Null";
        }
        //var_dump($parrentId); die();
        $query = Db::getInstance()->prepare("SELECT * FROM `comments` WHERE `theme_id` = $themeId AND `parrent_id` = $parrentId ORDER BY `date` DESC");


            $query->execute();
            $comments = $query->fetchAll();                                     //returns an array
            return $comments;
    }

    /*function getCategory() {
        $query = Db::getInstance()->prepare("SELECT * FROM `comments`");
        $result = array();
        while ($row = mysql_fetch_array($query)) {
            $result[$row["parent_id"]][] = $row;
        }
        return $result;*/



}

