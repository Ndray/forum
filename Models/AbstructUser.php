<?php

namespace Forum\Models;


abstract class AbstructUser
{
    public $id;
    public $name;
    public $email;
    protected $password;
    private $properties = ["id", "name", "email"];

    abstract public function setPassword(string $password);

    abstract public function getPassword(): string;

    abstract public function changePassword(string $oldPassword, string $newPassword);

}
