<?php

namespace Forum\Models;



class User extends AbstructUser
{
    private $status = 'active';
    protected const TYPE = 'user';
    private const SALT = 'ccfo58d3311s$';

    public function __construct(int $id, string $name, string $email, string $password = null)
    {
        if (!empty($id)) {
            $this->id = $id;
        }
        $this->name = $name;
        $this->email = $email;
        if (isset($password)) {
            $this->setPassword($password);
        }
    }

    public function __destruct()
    {
        // TODO: Implement __destruct() method.
    }

    public function setPassword(string $password)
    {
        $this->password = self::encryptPass($password);
    }

    private static function encryptPass(string $password): string
    {
        return sha1($password . self::SALT);
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function changePassword(string $oldPassword, string $newPassword): bool
    {
        if (self::encryptPass($oldPassword) === $this->password) {
            $this->password = self::encryptPass($newPassword);
            //sending email
            // self::sendEmail();
            return true;
        } else {
            return false;
        }
    }

    public static function login(string $email, string $password): array
    {
        $stmt = Db::getInstance()->prepare("
            SELECT
                *
            FROM
                `users`
            WHERE
                `email` = :email
            AND `password` = :password"
        );
        $stmt->execute(
            [
                "email" => $email,
                "password" => self::encryptPass($password),
            ]
        );
        $user = $stmt->fetch();
        return (!empty($user) ? $user : []);
    }

    public static function register(string $name, string $email, string $password, $type = self::TYPE): User
    {
        $stmt = Db::getInstance()->prepare("
            INSERT INTO `users` (
                `name`,
                `email`,
                `password`,
                `type`
            ) 
            VALUES
                (
                    :name,
                    :email,
                    :password,
                    :type
                )"
        );
        $stmt->execute(
            [
                "name" => $name,
                "email" => $email,
                "password" => self::encryptPass($password),
                "type" => $type
            ]
        );
        $id = Db::getInstance()->lastInsertId();
        return new User($id, $name, $email, $password);
    }

    public function __get(string $name): string
    {
        if ($name == 'password') {
            return $this->getPassword();
        }
    }

    public function __set(string $name,  string $value ): void
    {
        if($name == 'password'){
            $this->setPassword($value);
        } else {
            $this->{$name} = $value;
        }
    }

    public function __toString(): string
    {
        return $this->name . " " . $this->email;
    }

    protected function changeStatus(string $status): void
    {
        $this->status = $status;
    }

    public static function getUserByEmail(string $email)
    {
        $stmt = Db::getInstance()->prepare("
            SELECT
                *
            FROM
                `users`
            WHERE
                `email` = :email"
        );
        $stmt->execute(
            [
                "email" => $email,
            ]
        );
        $res = $stmt->fetch();
        if (empty($res)) {
            return false;
        }
        $user = new User($res['id'], $res['name'], $res['email']);
        $user->password = $res['password'];
        return $user;
    }


}