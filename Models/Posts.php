<?php

namespace Forum\Models;

class Posts
{
    public $id;
    public $postContent;
    public $description;
    public $date;
   // public $postTopic;
    //protected $properties = ["id", "postContent", "postTopic"];

    public function __construct(int $id, string $postContent, string $description, $date) {
        $this->id = $id;
        $this->postContent = $postContent;
        $this->description = $description;
        $this->date = $date;
        //$this->postTopic = $postTopic;

    }

    public static function getAll(): array
    {
       // $postsNew = [];
        $stmt = Db::getInstance()->query("
            SELECT
                *
            FROM
                `posts`"
        );
        $posts = $stmt->fetchAll();
        foreach ($posts as $post) {
            $postsNew [] = new Posts(
                $post['id'],
                $post['post_content'],
                $post['description'],
                $post['date']
                //$post['post_topic']
            );
        }
        return $postsNew;
    }

    /*public function postAndComment()
    {
        $stmt = Db::getInstance()->prepare('SELECT * FROM posts  WHERE id = :id');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);
    }*/

    public static function getById($id): array
    {
        //var_dump($id);
        $stmt = Db::getInstance()->prepare("
            SELECT
                *
            FROM
                `posts`
            WHERE 
                `id` = :id"
        );
        $stmt->execute(
            [
                "id" => $id
            ]
        );
        $post [] = $stmt->fetch();
        echo "<pre>";         //ПРИНТОМ ПОКАЗЫВАЕТ ВСЕ ЧТО НУЖНО, А РЕТЁРН ВЫДАЕТ  НЕ ВСЕ
        print_r($post);
        return $post;
    }






}